#!/usr/bin/env python3
import time
import os
import sys
import re
import subprocess
import getpass


def callProcess(cmd, live_output=False, printcmd=False, curdir="/", valid_returncodes=[0,], root=False, inc_returncode=False):
	output = ""

	if printcmd:
		print(cmd)

	if root and getpass.getuser() != "root":
			cmd = "sudo " + cmd

	process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, cwd=curdir)

	if live_output:
		for line in iter(process.stdout.readline, ''):
			if len(line) > 0:
				print(line.decode("ascii", "ignore").rstrip("\n"))
				output = output + line.decode("ascii", "ignore")
			else:
				break

		process.communicate()
	else:
		data   = process.communicate()
		output = data[0].decode("utf-8")

	if not process.returncode in valid_returncodes:
		raise subprocess.CalledProcessError(process.returncode, cmd=cmd, output=output)

	if inc_returncode:
		return (process.returncode, output)
	else:
		return output

def IsOnline(ip):
	try:
		rc = callProcess("ping -c 2 %s" % (ip), inc_returncode=True, valid_returncodes=[0,1])

		if rc[0] == 0:
			return True
		else:
			return False

	except subprocess.CalledProcessError:
		print("failed to call ping command")

		return False

def Wake(mac):
	callProcess("etherwake %s" % mac, root=True)


prev_time    = int(time.time())
sync_ip      = "192.168.0.1"
sync_mac     = "14:da:e9:4f:88:2c"
wake_timeout = 180

# We have been poked, release the kracken...
Wake(sync_mac)

timeout  = int(time.time()) + wake_timeout
cur_time = int(time.time())

while cur_time < timeout:
    if IsOnline(sync_ip):
        break

    cur_time = int(time.time())

    if not IsOnline(sync_ip):
        time.sleep(60)
        continue

if cur_time >= timeout:
	print("Failed to wake %s" % sync_ip)